# OpenML dataset: puma32H

https://www.openml.org/d/308

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
  
**Source**: Unknown -   
**Please cite**:   

This is one of a family of datasets synthetically generated from a realistic simulation of the dynamics of a Unimation Puma 560 robot arm. There are eight datastets in this family . In this repository we only have two of them. They are all variations on the same model; a realistic simulation of the dynamics of a Puma 560 robot arm. The task in these datasets is to predict the angular accelaration of one of the robot arm's links. The inputs include angular positions, velocities and torques of the robot arm. The family has been specifically generated for the delve environment and so the individual datasets span the corners of a cube whose dimensions represent:

Number of inputs 32 
degree of non-linearity (fairly linear or non-linear) 
amount of noise in the output (moderate or high). 

Source: collection of regression datasets by Luis Torgo (torgo@ncc.up.pt) at
http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
Original Source: DELVE repository of data. 
Characteristics: 8192 (4500+3692) cases, 33 continuous variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/308) of an [OpenML dataset](https://www.openml.org/d/308). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/308/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/308/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/308/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

